// ==UserScript==
// @name         ENS 4 Digits Checker
// @namespace    https://kudasaijp.com/
// @version      0.2
// @description  ENS Checker
// @author       5000e12
// @license      MIT License
// @match        https://opensea.io/collection/ens*
// @run-at       document-idle
// @grant        none
// ==/UserScript==

(() => {
  'use strict';

  const cache = new Map();

  async function addTokens(element) {
    const name = element.querySelector('img');
    const domain = name.alt.replace(/\.[^/.]+$/, "");
    if (domain.search(/^[-]?[0-9]+$/) === 0) {
      element.style.border = 'solid blue';
    }
    else if (domain.search(/^[-]?[0-9l]+$/) === 0) {
      element.style.border = 'solid red';
    } else {
      element.innerHTML = '';
    }
  }

  document.querySelectorAll('[role="gridcell"]').forEach(addTokens);

  new MutationObserver((mutations) => {
    for (const mutation of mutations) {
      if (mutation.addedNodes.length) {
        addTokens(mutation.addedNodes[0]);
      }
    }
  }).observe(
    document.querySelector('[role="grid"]'),
    { childList: true }
  );
})();
